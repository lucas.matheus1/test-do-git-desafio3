package DesafioMod3.dominio;

import DesafioMod3.sistemaGerenciamento.Morador;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Imovel{

    //Atributos...
    private double valorDeVenda;
    private String endereco;
    private double valorDoAluguel;

    //Atributo List onde o corpo e o morador e onde terá a lista de moradores.
    public static List<Morador> listaDeMoradoresEmImoveis = new ArrayList<>();

    //Metódo construtor...
    public Imovel(double valorDeVenda, String endereco, double valorDoAluguel) {
        this.valorDeVenda = valorDeVenda;
        this.endereco = endereco;
        this.valorDoAluguel = valorDoAluguel;
    }

    //Metódo Seletores e Modificadores...
    public double getValorDeVenda() {
        return valorDeVenda;
    }

    public void setValorDeVenda(double valorDeVenda) {
        this.valorDeVenda = valorDeVenda;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public double getValorDoAluguel() {
        return valorDoAluguel;
    }

    public void setValorDoAluguel(double valorDoAluguel) {
        this.valorDoAluguel = valorDoAluguel;
    }


    //Metódo para adicionar um morador em um imovel...
    public void adicionarMoradorNoImovel(Morador morador){

        System.out.println("Morador Adicionado! ");
        System.out.println("Entraremos em contato! ");
        System.out.println(" ");
        listaDeMoradoresEmImoveis.add(morador);
    }

    //Metódo para mostrar o morador do imovel
    public void mostrarMoradorDoImovel() {

        for (int i = 0; i < listaDeMoradoresEmImoveis.size(); i++) {

            System.out.println("Lista de Morador no Imovel");
            System.out.println(listaDeMoradoresEmImoveis.get(i));
            System.out.println();

        }
    }

    //Metódo para excluir morador
    public void excluirMorador(String cpf){

        // um for Iterator onde aprendi a remover de uma forma mais simples um dado de uma lista com minha mentora.
        for (Iterator<Morador> moradorIterator = listaDeMoradoresEmImoveis.iterator(); moradorIterator.hasNext();) {

            Morador morador = (Morador) moradorIterator.next();

            if (morador.getMorador().getCpf().equals(cpf)) {

                System.out.println("Morador Removido. ");
                moradorIterator.remove();

            }

        }
    }

    @Override
    public String toString() {
        System.out.println("Dados do Imovel ");
        System.out.println("Valor do Imovel: " + valorDeVenda);
        System.out.println("Endereço: " + endereco);
        System.out.println("Valor do Aluguel: " + valorDoAluguel);
        return " ";
    }
}
